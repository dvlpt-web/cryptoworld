<?php

namespace App\Form;

use App\Entity\Users;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;



class EditUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'constraints'=>[
                    new NotBlank([
                        'message'=>'Saisir une adresse email'
                    ])
                ],
                'required'=>true,
                'attr'=>[
                    'class'=>'form-control '
                ]
            ])
            ->add('roles', ChoiceType::class, [
                'choices'=>[
                    'Utilisateur'=>'ROLE_USER',
                    'Administrateur'=>'ROLE_ADMIN'
                ],
                'multiple'=>true,
                'expanded'=>true,
                
            ])
            ->add('Enregistrer', SubmitType::class, [
                'attr'=>[
                    'class'=>'btn btn-primary button'
                    ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Users::class,
        ]);
    }
}
