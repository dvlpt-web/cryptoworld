<?php

namespace App\Controller;

use App\Entity\Annonce;
use App\Entity\Commentaire;
use App\Form\AnnonceType;
use App\Form\CommentaireType;
use App\Entity\Users;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Repository\AnnonceRepository;

class AnnonceController extends AbstractController
{
    /**
     * @Route("/annonce", name="app_annonce")
     */
    public function index(): Response
    {
        return $this->render('annonce/index.html.twig', [
            'controller_name' => 'AnnonceController',
        ]);
    }
    /**
     * @Route("/listAnnonceUser", name="annonces_user")
     * @IsGranted("ROLE_USER")
     */
    public function listAnnonce(): Response
    {
        $annonces=$this->getDoctrine()->getRepository(Annonce::class)->getAnnonceById($this->getUser());
        return $this->render('annonce/userAnnonce.html.twig', [
            'annonces' => $annonces,
        ]);
    }

    /**
     * @Route("/annonce/ajout", name="ajout_annonce")
     * @IsGranted("ROLE_USER")
     */
    public function ajoutArticle(Request $request){
        $annonce= new Annonce();

        $form=$this->createForm(AnnonceType::class, $annonce);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $annonce->setUsers($this->getUser());
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($annonce);
            $doctrine->flush();

            return $this->redirectToRoute('app_crypto_blog');
        }
        return $this->render('annonce/ajout.html.twig', [
            'annonceForm'=>$form->createView()
        ]);
    }
    /**
     * @Route("/show/{slug}", name="show_annonce")
     */
    public function show(Annonce $annonce, Request $request)
    {
        $commentaire = new Commentaire();
        $form=$this->createForm(CommentaireType::class, $commentaire);
        $form->handleRequest($request);
        $commentaires = $this->getDoctrine()
            ->getRepository(Commentaire::class)
            ->getCommentByAnnonce($annonce->getId()
            );

        if($form->isSubmitted() && $form->isValid()){
            $commentaire->setUsers($this->getUser());
            $commentaire->setCreatedAt(new \DateTime());
            $commentaire->setAnnonce($annonce);
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($commentaire);
            $doctrine->flush();
        }

        return $this->render('annonce/show.html.twig', [
            'annonce' => $annonce,
            'commentaireForm'=>$form->createView(),
            'commentaires' => $commentaires
        ]);
    }
    /**
     * @Route("/edit/{slug}", name="annonce_edit")
     * @IsGranted("ROLE_USER")
     */
    public function edit(Annonce $annonce, Request $request){

        $form=$this->createForm(AnnonceType::class, $annonce);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em=$this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute('annonces_user');
        }
        return $this->render('annonce/edit.html.twig', [
            'annonceForm'=>$form->createView()
        ]);
    }
    /**
     * @Route("/delete/{slug}", name="annonce_delete")
     * @IsGranted("ROLE_USER")
     */
    
     public function delete(Annonce $annonce){

        $em = $this->getDoctrine()->getManager();
        $em->remove($annonce);
        $em->flush();
        return $this->redirectToRoute('annonces_user'); 
    }
}
