<?php

namespace App\Controller;

use App\Entity\Annonce;
use App\Entity\Users;
use App\Entity\Crypto;

use App\Repository\AnnonceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\FiltreType;
use Symfony\Component\HttpFoundation\Request;

class CryptoBlogController extends AbstractController
{
    /**
     * @Route("/", name="app_crypto_blog")
     */
    public function index(Request $request): Response
    {
        $annonce= new Annonce();
        $form=$this->createForm(FiltreType::class, $annonce);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            	
            $nom = $form->get('titre')->getData();
            $annonces=$this->getDoctrine()->getRepository(Annonce::class)->getAnnonceByCrypto($nom);
        }
        else{
            $annonces=$this->getDoctrine()->getRepository(Annonce::class)->findAll();
        }
        return $this->render('crypto_blog/index.html.twig', [
            'annonces' => $annonces,
            'filtreForm'=>$form->createView()
        ]);
    }
}
