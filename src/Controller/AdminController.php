<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UsersRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Users;
use App\Form\EditUserType;

/**
 * @Route("/admin", name="admin_")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * Liste des utilisateurs du site
     * @Route("/utilisateurs", name="utilisateurs")
     */
    
     public function usersList(UsersRepository $users){
         return $this->render('admin/usersList.html.twig', [
             'users'=>$users->findAll()
         ]);
     }

     /**
      * Modifier un utilisateur
      *
      * @route("/utilisateur/modifier/{id}", name="edit_user")
      */
     public function editUser(Users $user, Request $request){
        $form=$this->createForm(EditUserType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($user);
            $doctrine->flush();

            $this->addFlash('message', 'Utilisateur modifié avec succès');
            return $this->redirectToRoute('app_crypto_blog');
        }

        return $this->render('admin/edituser.html.twig', [
            'userForm'=>$form->createView()
        ]);
     }
}
