<?php

namespace App\DataFixtures;

use App\Entity\Annonce;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AnnonceFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $a1 = new Annonce();
        $a1->setUsers($manager->merge($this->getReference('u6')))
            ->setCrypto($manager->merge($this->getReference('c2')))
            ->setCreatedAt(new \DateTime('-5 days'))
            ->setTitre("L'etherium, valeur sûre")
            ->setDescription("Eh les crypto-bros, vous avez entendu parler de l'Ethereum ? C'est comme du deutérium, 
            mais en mieux car c'est une crypto sur Binance, avec mon code \"Jul13Jol13\"");


        $a2 = new Annonce();
        $a2->setUsers($manager->merge($this->getReference('u1')))
            ->setCrypto($manager->merge($this->getReference('c2')))
            ->setCreatedAt(new \DateTime('-5 days +1 hour'))
            ->setTitre("Venez sur mon lien pour l'Ethereum plutôt")
            ->setDescription("N'écoutez pas Jul13Jol13, allez pas chercher vos cryptos sur crypto.com avec mon lien 
            sponso \"XxP0nz1xX\". A plus les bros");

        $a3 = new Annonce();
        $a3->setUsers($manager->merge($this->getReference('u3')))
            ->setCrypto($manager->merge($this->getReference('c4')))
            ->setCreatedAt(new \DateTime('-3 days'))
            ->setTitre("BNB paribas a rien faire ici ???")
            ->setDescription("Je comprend pas, on essaye de faire des cryptos pour décentraliser mais y a 
            une banque qui fait une crypto et on l'accepte ? Qu'est ce que ça veut dire ?");

        $a4 = new Annonce();
        $a4->setUsers($manager->merge($this->getReference('u7')))
            ->setCrypto($manager->merge($this->getReference('c1')))
            ->setCreatedAt(new \DateTime('-2 days'))
            ->setTitre("Bitcoin c'est super")
            ->setDescription("C'est super simple, j'en ai acheté au tout début en 2009. Bon ça ne valait rien 
            3 ans, mais ça c'était avant, maintenant je suis tellement riche je sais pas quoi faire de l'argent");

        $a5 = new Annonce();
        $a5->setUsers($manager->merge($this->getReference('u1')))
            ->setCrypto($manager->merge($this->getReference('c10')))
            ->setCreatedAt(new \DateTime('-1 days +5 hour'))
            ->setTitre("Avalanche c'est chan-mé")
            ->setDescription("Ici à Tombouctou on utilise que l'Avalanche entre nous, c'est cool un coup de
            pousse les broooos");

        $manager->persist($a1);
        $manager->persist($a2);
        $manager->persist($a3);
        $manager->persist($a4);
        $manager->persist($a5);

        $manager->flush();

        $this->addReference('a1',$a1);
        $this->addReference('a2',$a2);
        $this->addReference('a3',$a3);
        $this->addReference('a4',$a4);
        $this->addReference('a5',$a5);
    }
    public function getDependencies(): array
    {
        return [
            UsersFixtures::class,
            CryptoFixtures::class,
        ];
    }
}
