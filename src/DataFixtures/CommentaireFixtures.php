<?php

namespace App\DataFixtures;

use App\Entity\Commentaire;
use App\Entity\Users;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CommentaireFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $c1 = new Commentaire();
        $c1->setUsers($manager->merge($this->getReference('u1')))
            ->setAnnonce($manager->merge($this->getReference('a1')))
            ->setCommentaire("Non venez plutôt sur crypto.com, j'ai un lien sponso à vous faire profiter 
            \"XxP0nz1xX\"")
            ->setCreatedAt(new \DateTime('-5 days +10 minute'))
        ;

        $c2 = new Commentaire();
        $c2->setUsers($manager->merge($this->getReference('u6')))
            ->setAnnonce($manager->merge($this->getReference('a1')))
            ->setCommentaire("Hein, mais j'ai mis mon code laisse moi tranquille, pas besoin de ton code 
            pourri")
            ->setCreatedAt(new \DateTime('-5 days +25 minute'))
        ;

        $c3 = new Commentaire();
        $c3->setUsers($manager->merge($this->getReference('u6')))
            ->setAnnonce($manager->merge($this->getReference('a2')))
            ->setCommentaire("Mais j'y crois pas, y a moyen de supprimer une annonce lesmodos ? C'est moi qui
            a eu l'idée :'(")
            ->setCreatedAt(new \DateTime('-5 days +1 hour +10 minute'))
        ;

        $c4 = new Commentaire();
        $c4->setUsers($manager->merge($this->getReference('u3')))
            ->setAnnonce($manager->merge($this->getReference('a2')))
            ->setCommentaire("mdr, get rekt Jul13")
            ->setCreatedAt(new \DateTime('-5 days +1 hour +15 minute'))
        ;

        $c5 = new Commentaire();
        $c5->setUsers($manager->merge($this->getReference('u2')))
            ->setAnnonce($manager->merge($this->getReference('a4')))
            ->setCommentaire("C'est pas genre, très volatil ?")
            ->setCreatedAt(new \DateTime('-3 days +8 hour'))
        ;

        $c6 = new Commentaire();
        $c6->setUsers($manager->merge($this->getReference('u5')))
            ->setAnnonce($manager->merge($this->getReference('a5')))
            ->setCommentaire("J'essaye de payer à Madrid avec mais ça marche pas, la chance :(")
            ->setCreatedAt(new \DateTime('-1 days +5 hour +6 minute'));

        $manager->persist($c1);
        $manager->persist($c2);
        $manager->persist($c3);
        $manager->persist($c4);
        $manager->persist($c5);
        $manager->persist($c6);

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            AnnonceFixtures::class,
            UsersFixtures::class
        ];
    }
}
