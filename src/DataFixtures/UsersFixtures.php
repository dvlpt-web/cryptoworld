<?php

namespace App\DataFixtures;

use App\Entity\Users;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UsersFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $u1 = new Users();
        $u1->setPseudo("TombouctouGang");
        $u1->setEmail("Tombouctou.Thomas@gmail.com");
        $u1->setPassword("\$argon2i\$v=19\$m=16,t=2,p=1\$N0VFNVFEUzN0SlJoVlVyag\$Sk/+OF0IUtTEjNPcR7nE8Q");
        $u1->setRoles(['ROLE_ADMIN']);

        $u2 = new Users();
        $u2->setPseudo("PresidentDesJeunes");
        $u2->setEmail("Emmanuel.Macron@laFronce.fr");
        $u2->setPassword("\$argon2i\$v=19\$m=16,t=2,p=1\$N0VFNVFEUzN0SlJoVlVyag\$X0w7aC05Vcws/4ROLhKFzg");

        $u3 = new Users();
        $u3->setPseudo("Stereo2lenfer");
        $u3->setEmail("Gilou.alastereo@hellfest.com");
        $u3->setPassword("\$argon2i\$v=19\$m=16,t=2,p=1\$N0VFNVFEUzN0SlJoVlVyag\$tma712E3il9Rr7l47vbfjg");

        $u4 = new Users();
        $u4->setPseudo("The Fame");
        $u4->setEmail("Lady.gaga@poker.com");
        $u4->setPassword("\$argon2i\$v=19\$m=16,t=2,p=1\$N0VFNVFEUzN0SlJoVlVyag$1k0US2ReQ9yMJV1JkZxLwQ");

        $u5 = new Users();
        $u5->setPseudo("Real_Crypto");
        $u5->setEmail("Karim.Benzema@real.madrid");
        $u5->setPassword("\$argon2i\$v=19\$m=16,t=2,p=1\$N0VFNVFEUzN0SlJoVlVyag\$dKQW8iomPZt1OcdwcWD4Cg");

        $u6 = new Users();
        $u6->setPseudo("Jul13Jol13");
        $u6->setEmail("Julie.Jolie@hotmail.fr");
        $u6->setPassword("\$argon2i\$v=19\$m=16,t=2,p=1\$N0VFNVFEUzN0SlJoVlVyag\$BXfc20be9XpyaUH/731Tsg");

        $u7 = new Users();
        $u7->setPseudo("Fonctionnaire_bord_du_gouffre");
        $u7->setEmail("paul.emploi@404.notfound");
        $u7->setPassword("\$argon2i\$v=19\$m=16,t=2,p=1\$N0VFNVFEUzN0SlJoVlVyag\$XGpurwCbDja8pj78rXVZqA");

        $manager->persist($u1);
        $manager->persist($u2);
        $manager->persist($u3);
        $manager->persist($u4);
        $manager->persist($u5);
        $manager->persist($u6);
        $manager->persist($u7);

        $manager->flush();

        $this->addReference('u1',$u1);
        $this->addReference('u2',$u2);
        $this->addReference('u3',$u3);
        $this->addReference('u4',$u4);
        $this->addReference('u5',$u5);
        $this->addReference('u6',$u6);
        $this->addReference('u7',$u7);
    }
}
