<?php

namespace App\DataFixtures;

use App\Entity\Liste;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ListeFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $l1 = new Liste();
        $l1->setUser($manager->merge($this->getReference('u1')));
        $l1->addCrypto($manager->merge($this->getReference('c1')));
        $l1->addCrypto($manager->merge($this->getReference('c3')));
        $l1->addCrypto($manager->merge($this->getReference('c7')));


        $l2 = new Liste();
        $l2->setUser($manager->merge($this->getReference('u2')));
        $l2->addCrypto($manager->merge($this->getReference('c4')));
        $l2->addCrypto($manager->merge($this->getReference('c5')));
        $l2->addCrypto($manager->merge($this->getReference('c2')));

        $l3 = new Liste();
        $l3->setUser($manager->merge($this->getReference('u3')));
        $l3->addCrypto($manager->merge($this->getReference('c1')));
        $l3->addCrypto($manager->merge($this->getReference('c2')));
        $l3->addCrypto($manager->merge($this->getReference('c6')));
        $l3->addCrypto($manager->merge($this->getReference('c7')));

        $l4 = new Liste();
        $l4->setUser($manager->merge($this->getReference('u4')));
        $l4->addCrypto($manager->merge($this->getReference('c2')));
        $l4->addCrypto($manager->merge($this->getReference('c3')));
        $l4->addCrypto($manager->merge($this->getReference('c4')));

        $l5 = new Liste();
        $l5->setUser($manager->merge($this->getReference('u5')));
        $l5->addCrypto($manager->merge($this->getReference('c1')));
        $l5->addCrypto($manager->merge($this->getReference('c2')));

        $l6 = new Liste();
        $l6->setUser($manager->merge($this->getReference('u6')));

        $l7 = new Liste();
        $l7->setUser($manager->merge($this->getReference('u7')));
        $l7->addCrypto($manager->merge($this->getReference('c6')));

        $manager->persist($l1);
        $manager->persist($l2);
        $manager->persist($l3);
        $manager->persist($l4);
        $manager->persist($l5);
        $manager->persist($l6);
        $manager->persist($l7);

        $manager->flush();

        $this->addReference('l1',$l1);
        $this->addReference('l2',$l2);
        $this->addReference('l3',$l3);
        $this->addReference('l4',$l4);
        $this->addReference('l5',$l5);
        $this->addReference('l6',$l6);
        $this->addReference('l7',$l7);
    }

    /**
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            UsersFixtures::class,
            CryptoFixtures::class,
        ];
    }
}
