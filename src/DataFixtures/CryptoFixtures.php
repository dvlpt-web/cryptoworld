<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Crypto;

class CryptoFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $c1 = new Crypto();
        $c1->setNom("Bitcoin");
        $c1->setPrix(42048.57);
        $c1->setCapMarche(798699520495);
        $c1->setVolume(18998431);
        $c1->setAccronyme("BTC");

        $c2 = new Crypto();
        $c2->setNom("Ethereum");
        $c2->setPrix(3034.01);
        $c2->setCapMarche(364071620821);
        $c2->setVolume(120178122);
        $c2->setAccronyme("ETH");

        $c3 = new Crypto();
        $c3->setNom("Tether");
        $c3->setPrix(0.8962);
        $c3->setCapMarche(73195934674);
        $c3->setVolume(81671735986);
        $c3->setAccronyme("USDT");

        $c4 = new Crypto();
        $c4->setNom("BNB");
        $c4->setPrix(397.35);
        $c4->setCapMarche(65342135714);
        $c4->setVolume(71922181690);
        $c4->setAccronyme("BNB");

        $c5 = new Crypto();
        $c5->setNom("USD Coin");
        $c5->setPrix(0.8963);
        $c5->setCapMarche(65342135714);
        $c5->setVolume(51769928333);
        $c5->setAccronyme("USDC");

        $c6 = new Crypto();
        $c6->setNom("XRP");
        $c6->setPrix(0.7698);
        $c6->setCapMarche(36870710071);
        $c6->setVolume(48121609012);
        $c6->setAccronyme("XRP");

        $c7 = new Crypto();
        $c7->setNom("Cardona");
        $c7->setPrix(1.07);
        $c7->setCapMarche(35962594749);
        $c7->setVolume(33739161873);
        $c7->setAccronyme("ADA");

        $c8 = new Crypto();
        $c8->setNom("Solana");
        $c8->setPrix(107.81);
        $c8->setCapMarche(34827758883);
        $c8->setVolume(325243869);
        $c8->setAccronyme("SOL");

        $c9 = new Crypto();
        $c9->setNom("Terra");
        $c9->setPrix(95.15);
        $c9->setCapMarche(33842660017);
        $c9->setVolume(355033219);
        $c9->setAccronyme("LUNA");

        $c10 = new Crypto();
        $c10->setNom("Avalanche");
        $c10->setPrix(86.29);
        $c10->setCapMarche(22903793216);
        $c10->setVolume(267271557);
        $c10->setAccronyme("BTC");

        $manager->persist($c1);
        $manager->persist($c2);
        $manager->persist($c3);
        $manager->persist($c4);
        $manager->persist($c5);
        $manager->persist($c6);
        $manager->persist($c7);
        $manager->persist($c8);
        $manager->persist($c9);
        $manager->persist($c10);

        $manager->flush();

        $this->addReference('c1',$c1);
        $this->addReference('c2',$c2);
        $this->addReference('c3',$c3);
        $this->addReference('c4',$c4);
        $this->addReference('c5',$c5);
        $this->addReference('c6',$c6);
        $this->addReference('c7',$c7);
        $this->addReference('c8',$c8);
        $this->addReference('c9',$c9);
        $this->addReference('c10',$c10);

    }
}
