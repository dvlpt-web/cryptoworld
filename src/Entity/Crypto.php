<?php

namespace App\Entity;

use App\Repository\CryptoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CryptoRepository::class)
 */
class Crypto
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $Prix;

    /**
     * @ORM\Column(type="bigint")
     */
    private $CapMarche;

    /**
     * @ORM\Column(type="bigint")
     */
    private $Volume;

    /**
     * @ORM\OneToMany(targetEntity=Annonce::class, mappedBy="crypto")
     */
    private $Annonces;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $accronyme;

    /**
     * @ORM\ManyToMany(targetEntity=Liste::class, mappedBy="Cryptos")
     */
    private $listes;

    public function __construct()
    {
        $this->Annonces = new ArrayCollection();
        $this->listes = new ArrayCollection();
    }

    public function __toString(){
        return $this->nom; 
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrix(): ?float
    {
        return $this->Prix;
    }

    public function setPrix(float $Prix): self
    {
        $this->Prix = $Prix;

        return $this;
    }

    public function getCapMarche(): ?string
    {
        return $this->CapMarche;
    }

    public function setCapMarche(string $CapMarche): self
    {
        $this->CapMarche = $CapMarche;

        return $this;
    }
    
    public function getVolume(): ?string
    {
        return $this->nom;
    }

    public function setVolume(string $Volume): self
    {
        $this->Volume = $Volume;

        return $this;
    }

    public function getAnnonces(): Collection
    {
        return $this->Annonces;
    }

    public function addAnnonce(Annonce $annonce): self
    {
        if (!$this->Annonces->contains($annonce)) {
            $this->Annonces[] = $annonce;
            $annonce->setAnnonce($this);
        }

        return $this;
    }

    public function removeAnnonce(Annonce $annonce): self
    {
        if ($this->Annonces->removeElement($annonce)) {
            // set the owning side to null (unless already changed)
            if ($annonce->getAnnonce() === $this) {
                $annonce->setAnnonce(null);
            }
        }

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAccronyme(): ?string
    {
        return $this->accronyme;
    }

    public function setAccronyme(string $accronyme): self
    {
        $this->accronyme = $accronyme;

        return $this;
    }

    /**
     * @return Collection<int, Liste>
     */
    public function getListes(): Collection
    {
        return $this->listes;
    }

    public function addListe(Liste $liste): self
    {
        if (!$this->listes->contains($liste)) {
            $this->listes[] = $liste;
            $liste->addCrypto($this);
        }

        return $this;
    }

    public function removeListe(Liste $liste): self
    {
        if ($this->listes->removeElement($liste)) {
            $liste->removeCrypto($this);
        }

        return $this;
    }

}
