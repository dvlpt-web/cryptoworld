<?php

namespace App\Entity;

use App\Repository\ListeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ListeRepository::class)
 */
class Liste
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Users::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\ManyToMany(targetEntity=Crypto::class, inversedBy="listes")
     */
    private $Cryptos;

    public function __construct()
    {
        $this->Cryptos = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?Users
    {
        return $this->User;
    }

    public function setUser(Users $User): self
    {
        $this->User = $User;

        return $this;
    }

    /**
     * @return Collection<int, Crypto>
     */
    public function getCryptos(): Collection
    {
        return $this->Cryptos;
    }

    public function addCrypto(Crypto $crypto): self
    {
        if (!$this->Cryptos->contains($crypto)) {
            $this->Cryptos[] = $crypto;
        }

        return $this;
    }

    public function removeCrypto(Crypto $crypto): self
    {
        $this->Cryptos->removeElement($crypto);

        return $this;
    }

}
